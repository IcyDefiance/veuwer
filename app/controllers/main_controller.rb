class MainController < ApplicationController
  MAX_FILE_SIZE = 10 * 1024 * 1024
  MAX_CACHE_SIZE = 128 * 1024 * 1024

  @links = Hash.new
  @files = Hash.new
  @tstamps = Hash.new
  @sizes = Hash.new
  @cache_size = 0

  class << self
    attr_accessor :links
    attr_accessor :files
    attr_accessor :tstamps
    attr_accessor :sizes
    attr_accessor :cache_size
  end

  def index
  end

  def transparency
  end

  def image
    @imglist = params[:id].split(',').map{ |id| ImageLink.find(id.to_i(36)) }
    @gvid_queue_pos = Hash.new
    @imglist.each do |link|
      if link.image.mimetype.split('/')[1] == 'gif' and !link.image.converted
        @gvid_queue_pos[link.image.id] = Image.where(mimetype: 'image/gif', converted: false).where('id <= ?', link.image.id).count
      end
    end
  end

  def gvid
    render html: "<html><head><title>Veuwer</title></head><body><video autoplay=\"autoplay\" muted=\"muted\" loop=\"loop\"><source src=\"/i/#{params[:id]}.webm\" type=\"video/webm\"><source src=\"/i/#{params[:id]}.mp4\" type=\"video/mp4\">Your browser does not support the video tag.</video></body></html>".html_safe
  end

  def image_direct
    linkid = params[:id].to_i(36)

    file =
      if Rails.env.production?
        Rails.cache.fetch("img#{linkid}", expires_in: 24.hours, race_condition_ttl: 10) do
          Aws::S3::Client.new.get_object(bucket: 'veuwer', key: "images/#{ImageLink.find(linkid).image.id.to_s(36)}.png").body.read
        end
      else
        File.open(Rails.root.join('uploads', "#{ImageLink.find(linkid).image.id.to_s(36)}.png"), 'rb') { |f| f.read }
      end

    send_data(file, type: ImageLink.find(linkid).image.mimetype, disposition: 'inline')
  end

  def mp4
    linkid = params[:id].to_i(36)
    imageid = ImageLink.find(linkid).image.id.to_s(36)

    file =
        if Rails.env.production?
          Rails.cache.fetch("mp4#{linkid}", expires_in: 24.hours, race_condition_ttl: 10) do
            Aws::S3::Client.new.get_object(bucket: 'veuwer', key: "videos/#{imageid}.mp4").body.read
          end
        else
          File.open(Rails.root.join('uploads', "#{imageid}.mp4"), 'rb') { |f| f.read }
        end

    send_data(file, type: 'video/mp4', disposition: 'inline')
  end

  def webm
    linkid = params[:id].to_i(36)
    imageid = ImageLink.find(linkid).image.id.to_s(36)

    file =
        if Rails.env.production?
          Rails.cache.fetch("webm#{linkid}", expires_in: 24.hours, race_condition_ttl: 10) do
            Aws::S3::Client.new.get_object(bucket: 'veuwer', key: "videos/#{imageid}.webm").body.read
          end
        else
          File.open(Rails.root.join('uploads', "#{imageid}.webm"), 'rb') { |f| f.read }
        end

    send_data(file, type: 'video/webm', disposition: 'inline')
  end

  def upload
    imgparam = params[:image]

    if imgparam.is_a?(String)
      name = File.basename(imgparam)
      imgpath = save_to_tempfile(imgparam).path
    else
      name = imgparam.original_filename
      imgpath = imgparam.tempfile.path
    end

    File.chmod(0666, imgpath)
    %x(/usr/local/bin/exiftool -all= -overwrite_original #{imgpath})
    logger.debug %x(which exiftool)
    render json: ImageManager.save_image(imgpath, name)
  end

private

  def save_to_tempfile(url)
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = uri.scheme == 'https'
    http.start do
      resp = http.get(uri.path)
      file = Tempfile.new('urlupload', Dir.tmpdir, :encoding => 'ascii-8bit')
      file.write(resp.body)
      file.flush
      return file
    end
  end
end
