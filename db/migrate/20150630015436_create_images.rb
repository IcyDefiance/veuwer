class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :imghash
      t.string :mimetype

      t.timestamps null: false
    end
  end
end
