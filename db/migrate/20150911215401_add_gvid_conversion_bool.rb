class AddGvidConversionBool < ActiveRecord::Migration
  def change
    add_column :images, :converted, :boolean, null: false, default: false
  end
end
