class CreateImageLinks < ActiveRecord::Migration
  def change
    create_table :image_links do |t|
      t.references :image, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
